import 'package:flutter/material.dart';
import 'registration.dart';
import 'login.dart';
import 'dashboard.dart';

import 'package:google_fonts/google_fonts.dart';
import 'package:splashscreen/splashscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        primarySwatch: Colors.yellow,
        fontFamily: 'OpenSans',
      ),
      home: SplashScreen(
        seconds: 7,
        navigateAfterSeconds:
            const RegistrationPage(title: 'Registration Page'),
        title: const Text(
          'SplashScreen',
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.black),
        ),
        backgroundColor: Colors.yellow,
      ),
    );
  }
}
