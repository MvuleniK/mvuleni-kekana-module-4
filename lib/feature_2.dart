import 'package:flutter/material.dart';
import 'dashboard.dart';
import 'feature_1.dart';

class FeatureTwoPage extends StatefulWidget {
  const FeatureTwoPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<FeatureTwoPage> createState() => _FeatureTwoPageState();
}

class _FeatureTwoPageState extends State<FeatureTwoPage> {
  void _changePage() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Text(
              'Feature 1',
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.yellow,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              IconButton(
                icon: const Icon(Icons.menu_open),
                color: Colors.white,
                onPressed: () {},
              ),
              const Text(
                'Dashboard',
                style: TextStyle(color: Colors.white),
              ),
            ]),
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              IconButton(
                icon: const Icon(Icons.menu_open),
                color: Colors.white,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              const DashboardPage(title: 'Dashboard Page')));
                },
              ),
              const Text(
                'Dashboard',
                style: TextStyle(color: Colors.white),
              ),
            ]),
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              IconButton(
                icon: const Icon(Icons.featured_play_list),
                color: Colors.white,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              const FeatureOne(title: 'Feature 1 Page')));
                },
              ),
              const Text(
                'Feature 1',
                style: TextStyle(color: Colors.white),
              ),
            ]),
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              IconButton(
                icon: const Icon(Icons.featured_play_list),
                color: Colors.white,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              const FeatureTwo(title: 'Feature 2 Page')));
                },
              ),
              const Text(
                'Feature 2',
                style: TextStyle(color: Colors.white),
              ),
            ]),
            Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              IconButton(
                icon: const Icon(Icons.person),
                color: Colors.white,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              const ProfilePage(title: 'Profile Page')));
                },
              ),
              const Text(
                'Profile',
                style: TextStyle(color: Colors.white),
              ),
            ]),
          ],
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
