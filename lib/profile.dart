import 'package:flutter/material.dart';
import 'dashboard.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  void _changePage() {
    setState(() {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const DashboardPage(title: 'Login Page')));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Username',
            ),
            const SizedBox(
              width: 600,
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter your new user name here',
                  labelText: 'User Name',
                ),
              ),
            ),
            const Text(
              'Email Address',
            ),
            const SizedBox(
              width: 600,
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter your new email address here',
                  labelText: 'Email Address',
                ),
              ),
            ),
            const Text(
              'Password',
            ),
            const SizedBox(
              width: 600,
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter you new password here',
                  labelText: 'Password',
                ),
                obscureText: true,
              ),
            ),
            const SizedBox(height: 30),
            ClipRRect(
                borderRadius: BorderRadius.circular(6),
                child: Stack(children: <Widget>[
                  Positioned.fill(
                    child: Container(
                      decoration: const BoxDecoration(color: Colors.yellow),
                    ),
                  ),
                  Tooltip(
                    message: 'Confirm your changes',
                    child: TextButton(
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(16.0),
                        primary: Colors.white,
                        textStyle: const TextStyle(fontSize: 20),
                      ),
                      onPressed: _changePage,
                      child: const Text('Confirm Changes'),
                    ),
                  ),
                ]))
          ],
        ),
      ),
    );
  }
}
